package dev.rehm.sorting;

import dev.rehm.models.Ingredient;
import dev.rehm.models.IngredientQuantityComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Driver {

    public static void main(String[] args) {
        Ingredient i1 = new Ingredient(3, "tbsp", "butter");
        Ingredient i2 = new Ingredient(2, "slices", "bread");
        Ingredient i3 = new Ingredient(4, "slices", "cheese");

        List<Ingredient> ingredientList1 = new ArrayList<>();
        ingredientList1.add(i1);
        ingredientList1.add(i2);
        ingredientList1.add(i3);

        Collections.sort(ingredientList1);

        for(Ingredient i: ingredientList1){
            System.out.println(i);
        }
        System.out.println("\n\n");
        Collections.sort(ingredientList1, new IngredientQuantityComparator());

        for(Ingredient i: ingredientList1){
            System.out.println(i);
        }
    }
}
