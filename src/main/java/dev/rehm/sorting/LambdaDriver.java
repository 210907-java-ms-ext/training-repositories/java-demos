package dev.rehm.sorting;

import dev.rehm.models.Ingredient;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class LambdaDriver {

    public static void main(String[] args) {
        Ingredient i1 = new Ingredient(3, "tbsp", "butter");
        Ingredient i2 = new Ingredient(2, "slices", "bread");
        Ingredient i3 = new Ingredient(4, "slices", "cheese");

        List<Ingredient> ingredientList1 = new ArrayList<>();
        ingredientList1.add(i1);
        ingredientList1.add(i2);
        ingredientList1.add(i3);

        Consumer<Ingredient> inlineConsumer = new Consumer<Ingredient>() {
            @Override
            public void accept(Ingredient ingredient) {
                System.out.println(ingredient.getQuantity() +" "+ingredient.getUnit()+" of "+ingredient.getName());
            }
        };

        Consumer<Ingredient> inlineConsumer2 = new Consumer<Ingredient>() {
            @Override
            public void accept(Ingredient ingredient) {
                System.out.println("processing an ingredient!!");
            }
        };

        /*
            Because we declare: Consumer<Ingredient> inlineConsumer3
            java is expecting an implementation of Consumer<Ingredient>
            - this means the accept method will look like
                void accept(Ingredient i){

                }
         */
        Consumer<Ingredient> inlineConsumer3 = (Ingredient ing) -> {System.out.println("processing an ingredient!!");;};


        // lambda is an inline implementation of a functional interface
        // 'i -> System.out.println(i)' is a lambda expression
        ingredientList1.forEach(i -> System.out.println(i));


    }
}

class IngredientConsumer implements Consumer<Ingredient> {

    @Override
    public void accept(Ingredient ingredient) {
        System.out.println(ingredient.getQuantity() +" "+ingredient.getUnit()+" of "+ingredient.getName());
    }
}
