package dev.rehm.streams;

import dev.rehm.models.Ingredient;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class StreamDriver {

    public static void main(String[] args) {
        List<Ingredient> ingredientList = Arrays.asList(
                new Ingredient(3, "tbsp", "butter"),
                new Ingredient(2, "slices", "bread"),
                null,
                new Ingredient(4, "slices", "cheese")
        );

        List<Ingredient> filteredList =
                ingredientList.stream()
                        .filter(i->i!=null && i.getUnit()!=null)
                        .filter(i->"slices".equals(i.getUnit()))
                        .collect(Collectors.toList());

        for(Ingredient ingredient: filteredList){
            System.out.println(ingredient);
        }

        List<String> stringIngredientList =
                ingredientList.stream()
                        .filter(i->i!=null)
                        .map(i->i.getQuantity()+" "+i.getUnit()+" of "+i.getName())
                        .peek(str-> System.out.println(str))  // intermediate operation
                        .collect(Collectors.toList());

        List<String> stringIngredientListEquivalent =
                ingredientList.stream()
                        .filter(Objects::nonNull)
                        .map(i->i.getQuantity()+" "+i.getUnit()+" of "+i.getName())
                        .peek(System.out::println)
                        .collect(Collectors.toList());

    }
}
