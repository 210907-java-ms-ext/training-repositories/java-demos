package dev.rehm.threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorDriver {

    public static void main(String[] args) {
//        Runnable job = ()-> System.out.println(Thread.currentThread().getName()+ " says hello");
//        Thread t = new Thread(job);
//        t.start();

        /*
        Runnable task1 = ()-> System.out.println("task-1 is running on " + Thread.currentThread().getName());
        Runnable task2 = ()-> System.out.println("task-2 is running on " + Thread.currentThread().getName());

        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();

        singleThreadExecutor.execute(task1);
        singleThreadExecutor.execute(task2);

        singleThreadExecutor.shutdown();
         */

        ExecutorService multiThreadExecutor = Executors.newFixedThreadPool(4);

        Runnable task1 = ()-> System.out.println("task-1 is running on " + Thread.currentThread().getName());
        Runnable task2 = ()-> System.out.println("task-2 is running on " + Thread.currentThread().getName());
        Runnable task3 = ()-> System.out.println("task-3 is running on " + Thread.currentThread().getName());
        Runnable task4 = ()-> System.out.println("task-4 is running on " + Thread.currentThread().getName());
        Runnable task5 = ()-> System.out.println("task-5 is running on " + Thread.currentThread().getName());
        Runnable task6 = ()-> System.out.println("task-6 is running on " + Thread.currentThread().getName());
        Runnable task7 = ()-> System.out.println("task-7 is running on " + Thread.currentThread().getName());
        Runnable task8 = ()-> System.out.println("task-8 is running on " + Thread.currentThread().getName());

        multiThreadExecutor.execute(task1);
        multiThreadExecutor.execute(task2);
        multiThreadExecutor.execute(task3);
        multiThreadExecutor.execute(task4);
        multiThreadExecutor.execute(task5);
        multiThreadExecutor.execute(task6);
        multiThreadExecutor.execute(task7);
        multiThreadExecutor.execute(task8);

        multiThreadExecutor.shutdown();

    }
}
