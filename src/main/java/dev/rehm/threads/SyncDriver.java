package dev.rehm.threads;

public class SyncDriver {

    public static void main(String[] args) {
        StringBuilder builder = new StringBuilder();
        StringBuffer buffer = new StringBuffer();

        // creating our task for the two threads
        StringTestRunnable job = new StringTestRunnable(buffer, builder);

        Thread t1 = new Thread(job);
        Thread t2 = new Thread(job);

        // starting each thread
        t1.start();
        t2.start();

        try {
            t1.join(); // making the current (main) thread wait until t1 is done executing its task
            t2.join(); // making the current (main) thread wait until t2 is done executing its task
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("StringBuilder: ");
        System.out.println(builder);
        System.out.println("\n\nStringBuffer: ");
        System.out.println(buffer);
    }

}

class StringTestRunnable implements Runnable{

    StringBuilder stringBuilder;
    StringBuffer stringBuffer;

    public StringTestRunnable(StringBuffer buffer, StringBuilder builder){
        super();
        this.stringBuffer = buffer;
        this.stringBuilder = builder;
    }

    @Override
    public void run(){
        for(int i=0; i<50; i++){
            this.stringBuilder.append("~");
            this.stringBuffer.append("~");
        }
    }

}

