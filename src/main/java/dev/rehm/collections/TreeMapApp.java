package dev.rehm.collections;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class TreeMapApp {

    public static void main(String[] args) {
        Map<String, String> ingredientMap = new TreeMap<>();
        ingredientMap.put("Cheese", "3 slices");
        ingredientMap.put("Bread", "2 slices");
        ingredientMap.put("Butter", "1 tbsp");

        System.out.println(ingredientMap.get("Bread"));

        Set<String> ingredients = ingredientMap.keySet();
        for(String i: ingredients){
            System.out.println(i);
        }

    }
}
