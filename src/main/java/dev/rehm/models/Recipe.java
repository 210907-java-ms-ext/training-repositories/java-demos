package dev.rehm.models;

import java.util.List;
import java.util.Objects;

public class Recipe implements Comparable<Recipe> {

    private int id;
    private List<Ingredient> ingredients;
    private String name;
    private List<String> steps;
    private String author;

    public Recipe() {
    }

    public Recipe(int id, List<Ingredient> ingredients, String name, List<String> steps, String author) {
        this.id = id;
        this.ingredients = ingredients;
        this.name = name;
        this.steps = steps;
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSteps() {
        return steps;
    }

    public void setSteps(List<String> steps) {
        this.steps = steps;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recipe recipe = (Recipe) o;
        return id == recipe.id && Objects.equals(ingredients, recipe.ingredients) && Objects.equals(name, recipe.name) && Objects.equals(steps, recipe.steps) && Objects.equals(author, recipe.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ingredients, name, steps, author);
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "id=" + id +
                ", ingredients=" + ingredients +
                ", name='" + name + '\'' +
                ", steps=" + steps +
                ", author='" + author + '\'' +
                '}';
    }

    @Override
    public int compareTo(Recipe o) {
        return Integer.compare(this.getId(), o.getId());
    }
}
