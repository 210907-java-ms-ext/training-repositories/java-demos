package dev.rehm.models;

import java.util.Comparator;

public class IngredientQuantityComparator implements Comparator<Ingredient> {

    @Override
    public int compare(Ingredient o1, Ingredient o2) {
        int quantityCheck = Double.compare(o1.getQuantity(), o2.getQuantity());
        if(quantityCheck!=0){
            return quantityCheck;
        }
        int unitCheck = o1.getUnit().compareTo(o2.getUnit());
        if(unitCheck!=0){
            return unitCheck;
        }
        return o1.getName().compareTo(o2.getName());
    }
}
