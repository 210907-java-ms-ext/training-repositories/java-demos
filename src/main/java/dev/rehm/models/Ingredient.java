package dev.rehm.models;

import java.util.Objects;

public class Ingredient implements Comparable<Ingredient> {

    private double quantity;
    private String unit;
    private String name;

    public Ingredient() {
    }

    public Ingredient(double quantity, String unit, String name) {
        this.quantity = quantity;
        this.unit = unit;
        this.name = name;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ingredient that = (Ingredient) o;
        return Double.compare(that.quantity, quantity) == 0 && Objects.equals(unit, that.unit) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantity, unit, name);
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "quantity=" + quantity +
                ", unit='" + unit + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int compareTo(Ingredient other) {
        return this.getName().compareToIgnoreCase(other.getName());
    }
}
